mednafen (1.24.1+dfsg-1) unstable; urgency=medium

  On PAL PS1, the pixel aspect ratio and lightgun crosshair position have
  been tweaked, which means that custom psx.xscale/psx.xscalefs settings
  may need to be adjusted, and lightgun-using games may need recalibration
  (in the game).

 -- Stephen Kitt <skitt@debian.org>  Wed, 18 Mar 2020 13:50:06 +0100

mednafen (1.22.1+dfsg-1) unstable; urgency=medium

  gzip-compressed ROM and disk images are now recognised by file
  extension, not header magic as in the past, so you may need to rename
  such images.

  “Game set” hashes are no longer supported, so save game files for
  multi-disc PC-FX games will need to be renamed before they will be seen
  in this version of Mednafen.

 -- Stephen Kitt <skitt@debian.org>  Sat, 02 Feb 2019 21:33:21 +0100

mednafen (1.21.3+dfsg-1) unstable; urgency=medium

  A number of important changes are liable to affect you if you’re
  upgrading from a 0.9 release.

  * Mednafen now only allows a single instance per base directory.
  * The configuration file is mednafen.cfg again; settings will be
    imported from mednafen-09x.cfg if it exists.
  * The input mapping setting format has changed; settings in the old
    format will be converted to the new one, *except* for keyboard
    mappings which will have to be rebuilt.
  * Scaling calculations have changed, and you might need to adjust xscale
    or yscale values if you use non-integer values.

 -- Stephen Kitt <skitt@debian.org>  Sat, 12 May 2018 15:42:42 +0200
